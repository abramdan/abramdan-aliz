import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.linear_model import LogisticRegression as LR
from sklearn.metrics import accuracy_score

class ThresholdBinarizer(BaseEstimator, TransformerMixin):  
    """Threshold optimization using Gini impurity minimization
    
    This class optimizes the threshold and transforms the probabilities using 
    this optimized threshold
    
    Attributes
    ----------
    classes_ : array, contains the two different classification classes
    
    threshold_ : float, the optimized probability threshold
    """
    
    def __init__(self, classes_):
        self.classes_ = classes_
        
    def fit(self, probas, y=None):
        """Optimize the threshold according to the given probabilities.
        
        Based on the probabilities and the classes the threshold level is
        optimized minimizing the Gini impurity
        If there are no class labesl provided, the threshold is set to its 
        default value which is 0.5
        If there are labels, we sort the probabilities and start iterating with
        the threshold that is between two consecutive probabilities.
        We calculate the Gini impurity which is the a measure of how often a 
        randomly chosen element from the set would be incorrectly labeled if 
        it was randomly labeled according to the distribution of labels in the 
        subset. 
        
        Parameters
        ----------
        probas : array-like, shape (n_samples, 2)
            This array contains the probabilites that the given instance
            belongs to one class or the other, where n_samples is the number 
            of samples.

        y : array-like, shape (n_samples,)
            Classification class labels.

        Returns
        -------
        self : object
            Returns self.
        """

        if y is None:
            self.threshold_ = 0.5
        else:
            gini_min = 2
            # c0 and c1 is the number of elements in the two classes
            c0 = len([i for i in y if i == self.classes_[0]])
            c1 = len(y) - c0
            # tc0: true c0, fc0: false c0
            tc0 = 0
            fc0 = 0
            # tc1: true c1, fc1: false c1
            tc1 = c1
            fc1 = c0

            data = np.hstack([probas, np.reshape(np.array(y, dtype=object), (len(y), 1))])
            data = sorted(data, key=lambda x: x[0])

            for ind in range(len(data) - 1):
                t = (data[ind][0] + data[ind + 1][0]) / 2
                if data[ind][2] == self.classes_[0]:
                    tc0 += 1
                    fc1 -= 1
                else:
                    fc0 += 1
                    tc1 -= 1

                gini = (((tc0 + fc0) / len(y)) * (tc0 / (tc0 + fc0) * (fc0 / (tc0 + fc0)) + 
                                                (fc0 / (tc0 + fc0) * (tc0 / (tc0 + fc0)))) + 
                       ((tc1 + fc1) / len(y)) * (tc1 / (tc1 + fc1) * (fc1 / (tc1 + fc1)) + 
                                                (fc1 / (tc1 + fc1) * (tc1 / (tc1 + fc1)))))

                if gini < gini_min:
                    gini_min = gini
                    self.threshold_ = t

        return self

    def transform(self, probas):
        """Transforms the given probabilities to class labels
               
        Parameters
        ----------
        probas : array-like, shape (n_samples, 2)
            This array contains the probabilites that the given instance
            belongs to one class or the other, where n_samples is the number 
            of samples.
        """
        try:
            getattr(self, "threshold_")
        except AttributeError:
            raise RuntimeError("You must fit the data to have the threshold optimized!")

        y_pred = [self.classes_[0] if i[0] > self.threshold_ else self.classes_[1] for i in probas]
        
        return(y_pred)

class custom_estimator(BaseEstimator, ClassifierMixin):  
    """Binary Logistic Regression classifier with threshold optimization using
    the Gini impurity.

    The Logistic Regression implementation is directly used from 
    sklearn.linear_model.LogisticRegression with the same parameters, except
    for the one regarding the multiclass option.

    Parameters
    ----------
    penalty : str, 'l1' or 'l2', default: 'l2'
        Used to specify the norm used in the penalization. The 'newton-cg',
        'sag' and 'lbfgs' solvers support only l2 penalties.

    dual : bool, default: False
        Dual or primal formulation. Dual formulation is only implemented for
        l2 penalty with liblinear solver. Prefer dual=False when
        n_samples > n_features.

    tol : float, default: 1e-4
        Tolerance for stopping criteria.

    C : float, default: 1.0
        Inverse of regularization strength; must be a positive float.
        Like in support vector machines, smaller values specify stronger
        regularization.

    fit_intercept : bool, default: True
        Specifies if a constant (a.k.a. bias or intercept) should be
        added to the decision function.

    intercept_scaling : float, default 1.
        Useful only when the solver 'liblinear' is used
        and self.fit_intercept is set to True. In this case, x becomes
        [x, self.intercept_scaling],
        i.e. a "synthetic" feature with constant value equal to
        intercept_scaling is appended to the instance vector.
        The intercept becomes ``intercept_scaling * synthetic_feature_weight``.

        Note! the synthetic feature weight is subject to l1/l2 regularization
        as all other features.
        To lessen the effect of regularization on synthetic feature weight
        (and therefore on the intercept) intercept_scaling has to be increased.

    class_weight : dict or 'balanced', default: None
        Weights associated with classes in the form ``{class_label: weight}``.
        If not given, all classes are supposed to have weight one.

        The "balanced" mode uses the values of y to automatically adjust
        weights inversely proportional to class frequencies in the input data
        as ``n_samples / (n_classes * np.bincount(y))``.

        Note that these weights will be multiplied with sample_weight (passed
        through the fit method) if sample_weight is specified.

    random_state : int, RandomState instance or None, optional, default: None
        The seed of the pseudo random number generator to use when shuffling
        the data.  If int, random_state is the seed used by the random number
        generator; If RandomState instance, random_state is the random number
        generator; If None, the random number generator is the RandomState
        instance used by `np.random`. Used when ``solver`` == 'sag' or
        'liblinear'.

    solver : {'newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'},
        default: 'liblinear'
        Algorithm to use in the optimization problem.

        - For small datasets, 'liblinear' is a good choice, whereas 'sag' and
            'saga' are faster for large ones.
        - For multiclass problems, only 'newton-cg', 'sag', 'saga' and 'lbfgs'
            handle multinomial loss; 'liblinear' is limited to one-versus-rest
            schemes.
        - 'newton-cg', 'lbfgs' and 'sag' only handle L2 penalty, whereas
            'liblinear' and 'saga' handle L1 penalty.

        Note that 'sag' and 'saga' fast convergence is only guaranteed on
        features with approximately the same scale. You can
        preprocess the data with a scaler from sklearn.preprocessing.

        .. versionadded:: 0.17
           Stochastic Average Gradient descent solver.
        .. versionadded:: 0.19
           SAGA solver.

    max_iter : int, default: 100
        Useful only for the newton-cg, sag and lbfgs solvers.
        Maximum number of iterations taken for the solvers to converge.

    verbose : int, default: 0
        For the liblinear and lbfgs solvers set verbose to any positive
        number for verbosity.

    warm_start : bool, default: False
        When set to True, reuse the solution of the previous call to fit as
        initialization, otherwise, just erase the previous solution.
        Useless for liblinear solver.

    n_jobs : int, default: 1
        Number of CPU cores used when parallelizing over classes if
        multi_class='ovr'". This parameter is ignored when the ``solver``is set
        to 'liblinear'. If given a value of -1, all cores are used.
        
    Attributes
    ----------

    clf_ : Logistic Regression Classifier using sklearn.linear_model.LogisticRegression

    tb_ : ThresholdBinarizer to optimize the threshold and transform probabilities
    """

    def __init__(self, penalty='l2', dual=False, tol=1e-4, C=1.0,
                 fit_intercept=True, intercept_scaling=1, class_weight=None,
                 random_state=None, solver='liblinear', max_iter=100,
                 verbose=0, warm_start=False, n_jobs=1):
   
        self.clf_ = LR(penalty='l2', dual=False, tol=1e-4, C=1.0, 
                       fit_intercept=True, intercept_scaling=1, class_weight=None,
                       random_state=None, solver='liblinear', max_iter=100,
                       verbose=0, warm_start=False, n_jobs=1)

    def fit(self, X, y=None):
        """Fits the model according to the given training data and creates the
        ThresholdBinarizer to optimize the threshold.
        
        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.

        y : array-like, shape (n_samples,)
            Target vector relative to X.        

        Returns
        -------
        self : object
            Returns self.
        """

        self.clf_.fit(X, y)
        proba = self.clf_.predict_proba(X)
        self.tb_ = ThresholdBinarizer(self.clf_.classes_).fit(proba, y)
    
        return self

    def predict(self, X):
        """Predict the class labels for the input data.
                
        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.
        """
        proba = self.clf_.predict_proba(X)
        result_classes = self.tb_.transform(proba)
        return(result_classes)

    def score(self, X, y):
        """Return the accuracy of the predictions
        
        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.

        y : array-like, shape (n_samples,)
            Target vector relative to X.   
        """
        y_pred = self.predict(X)
        accuracy = accuracy_score(y, y_pred)
        return(accuracy) 